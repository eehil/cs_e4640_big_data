# Assignment_0 Report

## Part 1 - Design

### 1. Interactions between main components in architecture of mysimbdp

![alt text](assignment-0-525352/images/diagram1.jpg "Figure 1: Data platform architecture")
<p>
I have designed the architecture of my simple big data platform as seen in Figure 1. The coreDMS is implemented as a MongoDB 
that is accessible by a Flask server that implements the API functions required by the coreDMS. The Flask server also fulfills 
the DaaS requirements as art of the assignment since it implements an API that "can be called by external 
dataproducers/consumers to store/read data into/from mysimbdp-coredms". In practice, the Flask server includes routes that allow 
importing and exporting data.
</p>

### 2. Number of nodes needed in the deployment of mysimbdp-coredms so that this component can work properly

<p>
The MongoDB cluter in our coredms deployment is required to have at least two nodes. This is because, in case of a failure, the
 secundary node can take the place of the primary node. This increases the availability of the service. In general, MongoDB
 replica set can have up to 50 nodes which of 8 are only voting members in the cluster. 
</p>

### 3. Use VMs or containers for mysimbdp
<p>
We will use (Docker) containers for myimbdp. We utilize containers for the whole platform (except dataingest) because containers
 enable platform indepency, resource efficiency, isolation, security and scalability. Dataingest is not containerized because it
 is not executed inside the platform and thus the previously mentioned benefits do not apply for it.
 </p>

### 4. How to scale mysimbdp to allow  a lot of users using mysimbdp-dataingest to push data into mysimbdp
<p>
Because mysimbdp-dataingest uses the APIs of the mysimbdp-daas, it does not really limit the number of users. However, each 
stakeholder should have their own credentials to authenticate to the platform. This way data privacy would be maintained.
</p>

### 5. Choice of industrial cloud infrastructure and/or mysimbdp-coredms provider, when you do not have enough infrastructural resources for provisioning mysimbdp
<p>
In this assignment, we are running the platform locallly on one PC but in reality the computational
 capabilities would limit the service quality as the number of clients increases. Therefore, use of 
 industrial-level cloud infrastructure would be required. In that case, I would probably choose to use
 something like Google's Kubernetes in Google Cloud. Kubernetes enables elasticity of computational
 resources and high availability accross the cloud platform. In addition, Google Cloud computing 
 services are affordable and easy to use.
</p>

## Part 2 - Development and deployment

### 1. Data structure for mysimbdp-coredms
![alt text](assignment-0-525352/images/diagram2.jpg "Figure 2: Database structure")
<p>
The data structure of mysimbdp-coredms follows the best practice structure of MongoDB databe (see figure 2).
</p>

### 2. How to partition the data in mysimbdp-coredms into different shards/partitions
<p>
Sharding big data might become useful, if the volume or velocity of data overcome the capacity of a single database node. In this 
case we have to partition the database into subsets that are called shards and are located on different nodes to distribute the load.
This method is called horizontal scaling  and it can be more affordable than vertical scaling that require one extra powerful node.
</p>

### 3. Write a mysimbdp-dataingest that takes data from your selected sources and stores the data into mysimbdp-coredms
<p>
I wrote myimbdp-dataingest as a Python script that loads a csv file from local memory and uses the platform API to upload
 the data into the database. The program code is located in assignment-o-525352/code/datasets/myimbdp_dataingest_upload.py . 
 Instructions are included in the README-file of the datasets/ folder.
</p>

### 4. Uploading performance (response time and failure)

| Clients | Response time<sup>1</sup> | Failures (%) |
| ------- | ------------------------- | ------------ |
|    1    |             4.05 s        |      0       |
|    5    |            28.13 s        |      0       |
|   10    |            56.84 s        |      6       |
|    n    |           ~5.5*n s        |    n / 2     |

<sup>1</sup> This response time is the average elapsed time of sending a 7.1 MB file ten times 
consecutively. 

### 5. Solution to performance and failure problems
<p>
Currently, the platform is only running in a single container and that's why it fails to serve several simultaneus
 clients adequetly. To avoid this problem, we could deploy the container in Kubernetes cluster that would 
 allow autoscaling of the deployment. This way the computational resources would increase when needed. Another 
 easier way would be to add the container to a Docker swarm that allow scaling of the containers as well.
</p>

## Part 3 - Extension with discovery

## 1. Data schema design of service information for mysimbdp-coredms
<p>
So, in this platform, we are utilizing MongoDB as the mysimbdp-coredms. We create a dedicated database
 for every tenant/user that is sharded across different worked nodes automatically. When we are creating a 
 new database for a tenant/user, we store the name of the database to an registry. In this platform, we will use
 etcd as the registry. It should be noted, however, that this kinda of system would require all sorts of 
 security protocols that would ensure information security (authentication, encryption...) but we 
 will not go into details on those aspects. MongoDB allows a felexible deployment for databases 
 and that's why we don't need extensive service information in the etcd database. 
 Here is the simple data schema for our etcd registry: <br>
</p>
 
```
databases/ {tenant name : database name}

location / {tenant name : location name}

sla / {tenant name : sla information}
``` 


## 2. Program for publishing the service information of mysimbdp-coredms into etcd
<p>
The tutorial for publishing service information was found from its [documentation](https://github.com/etcd-io/etcd).

1.  Install etcd
2.  mv /tmp/etcd-download-test/etcd /usr/local/bin/  && etcd
3.  Setting a key pair:
```
curl http://127.0.0.1:2379/v2/databases/tenant_name -XPUT -d value="database_name"
```
4. Getting a key pair:
```
curl http://127.0.0.1:2379/v2/databases/tenant_name
```
</p>

## 3. Changes to the implementation of mysimbdp-dataingest to integrate a service discovery feature
<p>
The service discovery feature could be implemented in the etcd since it can robustly maintain key-value pairs.
This way if the location of the server dynamically changes - the service is discoverable for the tenants/users.
</p>

## 4. API design for mysimbdp-daas to allow any other developer to implement their own mysimbdp-dataingest
<p>
Generally, we would need APIs for:

*  Creating a database
*  Deleting a database
*  Creating a collection (table)
*  Deleting a collection (table)
*  Inserting a document (row)
*  Querying a document (row) with parameters for output (sorting...)
*  Updating a document (row)
*  Deleting a document (row)
</p>

## 5. Changes to mysimbdp-dataingest to work with mysimbdp-daas
<p>
I would change the mysimbdp-dataingest to send http requests as the new API definitions require. However,
we have to not that this kind of heavy API that requires many calls would be a bad way to implement the 
wanted features.
</p>





