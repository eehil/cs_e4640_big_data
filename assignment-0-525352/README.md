# Assignment 0  525352

```
├── assignment-0-525352
│   ├── assignment-git.log
│   ├── code
│   │   ├── datasets
│   │   │   ├── listings.csv
│   │   │   ├── myimbdp_dataingest_upload_performance.py
│   │   │   ├── myimbdp_dataingest_upload.py
│   │   │   └── testscript.sh
│   │   ├── flask_server
│   │   │   ├── app.py
│   │   │   ├── docker-compose.yml
│   │   │   ├── Dockerfile
│   │   │   ├── init_database.py
│   │   │   ├── requirements.txt
│   │   │   ├── templates
│   │   │   │   └── hello.html
│   │   │   └── uploads
│   │   └── README.md
│   ├── data
│   │   └── listings.csv
│   ├── images
│   │   ├── diagram1.jpg
│   │   └── diagram2.jpg
│   ├── logs
│   │   └── performance.log
│   ├── README.md
│   ├── reports
│   │   └── Report.md
│   ├── selfgrading.csv
│   └── submitter.csv

```
<p>
The source code is located in /code. There the platforms heart is in /code/flask_server. Otherwise the 
contents are pretty much self-explanatory.

</p>




