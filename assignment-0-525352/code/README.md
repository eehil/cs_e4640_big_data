# Instructions to reproduce

<p>
Dependencies: linux os, Python, docker, etcd

Steps to run the project:

1. Install necessary dependencies.
2. To build and run the flask server:

```
cd code/flask_server && docker-compose build && docker-compose up

```
3. To upload data with dataingest:

```
cd code/datasets
```
and

```
python myimbdp_dataingest_upload.py
```
This command will upload the file listings.csv to the database. MongoDB will take care of database
 setup automatically.

4. To stop the server 'ctrl + c', will do the trick.


*  API config can be seen from code/flask_server/app.py
*  Script for testing the performance of data uploading is in code/datasets/testscript.sh
</p>
