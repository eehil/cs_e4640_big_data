import csv
import requests
import time

filename = "listings.csv"
values = {'DB': 'photcat', 'OUT': 'csv', 'SHORT': 'short'}

avg_time = 0
failures = 0

for i in range(1,11):
    files = {'file' : open(filename, 'rb')}
    r = requests.post('http://0.0.0.0:5001/uploadFile', files=files, data=values)
    avg_time += r.elapsed.total_seconds()
    if r.status_code != 200:
        failures += 1
    print(r.status_code)

avg_time = avg_time / 10
print("DONE!")
print("Avg:", avg_time, "Failures:", failures)
