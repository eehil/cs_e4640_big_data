import pymongo

db_name = "airbnb_database"
col_name = "listings"

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
dblist = myclient.list_database_names()
print(dblist)
if db_name in dblist:
  print("The database already exists.")
else:
  mydb = myclient[db_name]
  print("Database created!")

# creating collections for the db
mycol = mydb[col_name]
print("Collection created in the database")

# use this if you want to delete collection data
# mycol = mydb[col_name]
#mycol.drop() 