from flask import Flask, flash, request, redirect, url_for
from flask_pymongo import PyMongo
from flask import render_template
from flask import request
from pymongo import MongoClient
from werkzeug.utils import secure_filename
import os
import pandas as pd

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://localhost:27017/airbnb_database"
mongo = PyMongo(app)
collection = mongo.db.listings


# upload configurations
UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = set(['txt', 'csv'])
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
  return '.' in filename and \
    filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def csvtoMongo(filename):
  df = pd.read_csv(UPLOAD_FOLDER + '/' + filename) #csv file which you want to import
  records_ = df.to_dict(orient = 'records')
  collection.insert_many(records_ )


@app.route("/")
def home_page():
  retstring = "Collections in this database:\n"
  collection_list = mongo.db.list_collection_names()
  for col in collection_list:
    retstring += col + ' '
  return render_template("hello.html",name=retstring)

@app.route('/postData', methods = ['POST'])
def postDataHandler():
  if request.is_json:
    content = request.get_json()
    collection.insert_one(content)
    return 'Data inserted'
  else:
    return 'Error'

@app.route('/getData', methods = ['POST'])
def getDataHandler():
  if not request.is_json:
    return 'Parameter type not JSON'

  content = request.get_json()

  col = content['collection']

  if not col in mongo.db.list_collection_names():
    return 'Column not found'

  thecollection = mongo.db[col]
  row = thecollection.find_one()

  return str(row)

@app.route('/uploadFile', methods=['GET', 'POST'])
def upload_file():
  if request.method == 'POST':
    # check if the post request has the file part
    if 'file' not in request.files:
      flash('No file part')
      return redirect(request.url)
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
      flash('No selected file')
      return redirect(request.url)
    if file and allowed_file(file.filename):
      filename = secure_filename(file.filename)
      file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
      csvtoMongo(filename)
      return 'Success!'

    return 'Error'




if __name__== '__main__':
  app.run(port=8080)
