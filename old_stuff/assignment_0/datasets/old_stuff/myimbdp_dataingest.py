import csv
import requests

filename = "listings.csv"
csvfile = open(filename, 'r')
reader = csv.DictReader(csvfile)
header = ["id", "name", "host_id", "host_name",	"neighbourhood_group",
            "neighbourhood", "latitude", "longitude", "room_type",
            "price", "minimum_nights", "number_of_reviews",	"last_review",
            "reviews_per_month", "calculated_host_listings_count", 
            "availability_365"]


for each in reader:
    row = {}
    for field in header:
        row[field] = each[field]
    r = requests.post('http://localhost:8080/postData', json=row)

print("DONE!")